# sklunforced

The purpose of this repository is to store an alternative launcher for SKL (SKLauncher) without forced updates (at this point it is also without any automatic or semi-automatic updates at all).
So if you feel like your SKL setup works and you shouldn't have to update, use this.

## What you need

The way SKLauncher works is that you have the actual launcher for minecraft inside a file called sklauncher-fx.jar and what you are actually running when executing sklauncher.exe is actually a launcher FOR a launcher. this file will check for updates and refuse to do anything if a force flag is set by remote server. Generally, updates are automatic, unless the exe itself is updated, not the jar. This project aims to be an alternative only for that executable, so you will still need to get your hands on the sklauncher-fx.jar one way or another. Once you have it, put this program in whatever directory you want it to keep all of it's files and all of minecraft files as well, and then run it with 

```bash
java -jar skunforced.jar initonly
```

which should fail the first time as you have not copied the jar yet (but it will create some directories, so you don't have to). Then, put the jar file in the skl directory and things should work now!
Any argument that you pass to skunforced will be passed on to the launcher (except initonly). to verify that it indeed works, you can run the jar without any arguments and after downloading some javafx libraries you should see the sklauncher window.

## Problems

Depending on what version of java you use to compile and run this, the game might crash on startup and the logs might show errors related to swing classes, like JPanel. If this happens, tick the compatibility mode checkbox in the SKLauncher profile settings. It should work after that.