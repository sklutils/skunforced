package org.terrydavis.skunforced.injection;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class ClasspathUtil
{
    public static final ClassLoader scl;
    @SuppressWarnings("unused")
	private static Tree tree;
    
    public static Class<?> getSystemClass(final String className) throws ClassNotFoundException {
        return Class.forName(className, false, ClasspathUtil.scl);
    }
    
    public static boolean classExists(final String name) {
        try {
            getSystemClass(name);
            return true;
        }
        catch (Exception ex) {
            return false;
        }
    }
    
    public static boolean resourceExists(String path) {
        if (!path.startsWith("/")) {
            path = "/" + path;
        }
        return ClasspathUtil.class.getResource(path) != null;
    }
    
    public static InputStream resource(String path) {
        if (!path.startsWith("/")) {
            path = "/" + path;
        }
        return ClasspathUtil.class.getResourceAsStream(path);
    }
    
    static {
        scl = ClassLoader.getSystemClassLoader();
    }
    
    public static class Tree implements Comparable<Tree>
    {
        private final Tree parent;
        private final String value;
        private Map<String, Tree> children;
        private boolean frozen;
        
        public Tree(final Tree parent, final String value) {
            this.parent = parent;
            this.value = value;
        }
        
        @SuppressWarnings("unused")
		private void freeze() {
            this.frozen = true;
            if (this.children != null) {
                this.children.values().forEach(Tree::freeze);
            }
        }
        
        public Tree visit(final String child) {
            if (this.children == null) {
                this.children = new HashMap<String, Tree>();
            }
            if (this.frozen) {
                return this.children.get(child);
            }
            return this.children.computeIfAbsent(child, c -> new Tree(this, c));
        }
        
        public Tree visitPath(final String path) {
            final String[] parts = path.split("/");
            return this.visitPath(parts);
        }
        
        public Tree visitPath(final String... path) {
            Tree node = this;
            for (final String part : path) {
                final Tree subtree = node.visit(part);
                if (subtree == null) {
                    return null;
                }
                node = subtree;
            }
            return node;
        }
        
        public List<Tree> getBranches() {
            if (this.children == null) {
                return Collections.singletonList(this);
            }
            final List<Tree> values = new ArrayList<Tree>();
            this.children.values().stream().filter(Tree::isBranch).forEach(values::add);
            return values;
        }
        
        public List<Tree> getLeaves() {
            if (this.children == null) {
                return Collections.singletonList(this);
            }
            final List<Tree> values = new ArrayList<Tree>();
            this.children.values().stream().filter(Tree::isLeaf).forEach(values::add);
            return values;
        }
        
        public List<Tree> getAllLeaves() {
            if (this.children == null) {
                return Collections.singletonList(this);
            }
            final List<Tree> values = new ArrayList<Tree>();
            this.children.values().forEach(t -> values.addAll(t.getAllLeaves()));
            return values;
        }
        
        public Map<String, Tree> getChildren() {
            if (this.children == null) {
                return Collections.emptyMap();
            }
            return this.children;
        }
        
        public boolean isRoot() {
            return this.parent == null;
        }
        
        public boolean isLeaf() {
            return this.children == null;
        }
        
        public boolean isBranch() {
            return !this.isLeaf();
        }
        
        public Tree getParent() {
            return this.parent;
        }
        
        public String getValue() {
            return this.value;
        }
        
        public String getFullValue() {
            if (this.parent == null) {
                return this.getValue();
            }
            if (this.parent.isRoot()) {
                return this.getValue();
            }
            return this.parent.getFullValue() + "/" + this.getValue();
        }
        
        @Override
        public int compareTo(final Tree o) {
            return this.getFullValue().compareTo(o.getFullValue());
        }
        
        @Override
        public String toString() {
            return this.getFullValue();
        }
        
        @Override
        public boolean equals(final Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || this.getClass() != o.getClass()) {
                return false;
            }
            final Tree tree = (Tree)o;
            return Objects.equals(this.parent, tree.parent) && this.value.equals(tree.value);
        }
        
        @Override
        public int hashCode() {
            return Objects.hash(this.parent, this.value);
        }
    }
}

