package org.terrydavis.skunforced.injection;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadFactory;

public class ThreadPoolFactory
{
    private static final int MAX;
    
    public static ScheduledExecutorService newScheduledThreadPool(final String name) {
        return newScheduledThreadPool(name, true);
    }
    
    public static ScheduledExecutorService newScheduledThreadPool(final String name, final boolean daemon) {
        return Executors.newScheduledThreadPool(ThreadPoolFactory.MAX, new FactoryImpl(name, daemon));
    }
    
    static {
        MAX = Math.max(2, Runtime.getRuntime().availableProcessors() - 2);
    }
    
    private static class FactoryImpl implements ThreadFactory
    {
        private final String name;
        private final boolean daemon;
        private int tid;
        
        public FactoryImpl(final String name, final boolean daemon) {
            this.tid = 0;
            this.name = name;
            this.daemon = daemon;
        }
        
        @Override
        public Thread newThread(final Runnable r) {
            final Thread thread = new Thread(r);
            thread.setDaemon(this.daemon);
            thread.setName(this.name + "-" + this.tid++);
            return thread;
        }
    }
}

