package org.terrydavis.skunforced.injection;

import java.util.function.Supplier;

public class Unchecked
{
    public static void run(final UncheckedRunnable runnable) {
        runnable.run();
    }
    
    public static <T> Supplier<T> supply(final UncheckedSupplier<T> supplier) {
        return supplier;
    }
}
