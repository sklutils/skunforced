package org.terrydavis.skunforced.injection;

import org.terrydavis.skunforced.reflectutils.ReflectUtil;

@FunctionalInterface
public interface UncheckedRunnable extends Runnable
{
    default void run() {
        try {
            this.uncheckedRun();
        }
        catch (Throwable t) {
            try {
				ReflectUtil.propagate(t);
			} catch (Throwable e) {
				e.printStackTrace();
			}
        }
    }
    
    void uncheckedRun() throws Throwable;
}

