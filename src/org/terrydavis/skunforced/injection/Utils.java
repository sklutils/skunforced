package org.terrydavis.skunforced.injection;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.OpenOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.util.Collections;
import java.util.Formatter;
import java.util.List;

public class Utils
{
    
    public static String sha1(final File file) {
        try {
            final MessageDigest messageDigest = MessageDigest.getInstance("SHA1");
            try (final InputStream is = new BufferedInputStream(Files.newInputStream(file.toPath(), new OpenOption[0]))) {
                final byte[] buffer = new byte[1024];
                int read;
                while ((read = is.read(buffer)) != -1) {
                    messageDigest.update(buffer, 0, read);
                }
            }
            try (final Formatter formatter = new Formatter()) {
                for (final byte b : messageDigest.digest()) {
                    formatter.format("%02x", b);
                }
                return formatter.toString();
            }
        }
        catch (Exception ex2) {
            System.out.println("Error: " + ex2.getMessage());
            ex2.printStackTrace();
            return null;
        }
    }
    
    public static void writeStringToFile(final File file, final String string) throws IOException {
        final List<String> lines = Collections.singletonList(string);
        final Path path = Paths.get(file.toURI());
        Files.write(path, lines, StandardCharsets.UTF_8);
    }
    
    public static void createRawFile(final File destination) throws Exception {
        final File parentFile = new File(destination.getParent());
        if (!parentFile.exists() && !parentFile.mkdirs()) {
            throw new Exception("Unable to create directory");
        }
        if (destination.exists() && !destination.delete()) {
            throw new Exception("Couldn't delete previous files (" + destination.getName() + ") Make sure, another Launcher or Minecraft is not running");
        }
        if (!destination.createNewFile()) {
            throw new Exception("Couldn't create the new file (" + destination.getName() + ") Make sure, another Launcher or Minecraft is not running");
        }
    }
    
    public static boolean compareHashes(final String hash1, final String hash2) {
        return hash1 != null && !hash1.isEmpty() && hash2 != null && !hash2.isEmpty() && hash1.trim().equalsIgnoreCase(hash2.trim());
    }
}
