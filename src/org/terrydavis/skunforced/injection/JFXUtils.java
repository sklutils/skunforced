package org.terrydavis.skunforced.injection;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.concurrent.atomic.AtomicBoolean;

import org.terrydavis.skunforced.reflectutils.JavaVersion;

public class JFXUtils
{
    @SuppressWarnings("unused")
	private static final String BASE_PLATFORM = "javafx.application.Platform";
    private static final AtomicBoolean initialized;
    
    public static void initializePlatform(final Runnable init) {
        if (!JFXUtils.initialized.compareAndSet(false, true)) {
            throw new IllegalStateException("Already initialized!");
        }
        try {
            final Method m = Class.forName(getPlatformClassName()).getDeclaredMethod("startup", Runnable.class);
            m.setAccessible(true);
            m.invoke(null, (Runnable)() -> {
                try {
                    init.run();
                }
                finally {
                    onInitializePlatform();
                }
            });
        }
        catch (NoSuchMethodException ex) {
            throw new IllegalStateException("javafx.application.Platform.startup(Runnable) is missing", ex);
        }
        catch (IllegalAccessException ex2) {
            throw new IllegalStateException("'startup' became inaccessible", ex2);
        }
        catch (InvocationTargetException ex3) {
            throw new IllegalStateException("Unable to initialize toolkit", ex3.getTargetException());
        }
        catch (ClassNotFoundException ex4) {
            throw new IllegalStateException("Platform class does not contain 'startup' method", ex4);
        }
    }
    
    public static String getPlatformClassName() {
        return (JavaVersion.get() >= 9) ? "javafx.application.Platform" : "com.sun.javafx.application.PlatformImpl";
    }
    
    private static void onInitializePlatform() {
        System.out.printf(String.format("JavaFX platform initialized from: %s%n", getPlatformClassName()));
    }
    
    static {
        initialized = new AtomicBoolean();
    }
}
