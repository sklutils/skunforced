package org.terrydavis.skunforced.injection;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.Collections;
import java.util.List;

import org.terrydavis.skunforced.reflectutils.JavaVersion;
import org.terrydavis.skunforced.reflectutils.ReflectUtil;

public class ClassLoaderInternals
{
    public static Object getUcp() throws ReflectiveOperationException {
        final Class<?> clsClassLoaders = Class.forName("jdk.internal.loader.ClassLoaders");
        final Object appClassLoader = clsClassLoaders.getDeclaredMethod("appClassLoader", (Class<?>[])new Class[0]).invoke(null, new Object[0]);
        Class<?> ucpOwner = appClassLoader.getClass();
        if (JavaVersion.get() >= 16) {
            ucpOwner = ucpOwner.getSuperclass();
        }
        final Field fieldUCP = ReflectUtil.getDeclaredField(ucpOwner, "ucp");
        return fieldUCP.get(appClassLoader);
    }
    
    @SuppressWarnings("unchecked")
	public static List<URL> getUcpPathList(final Object ucp) throws ReflectiveOperationException {
        if (ucp == null) {
            return Collections.emptyList();
        }
        final Class<?> ucpClass = ucp.getClass();
        final Field path = ReflectUtil.getDeclaredField(ucpClass, "path");
        return (List<URL>)path.get(ucp);
    }
    
    public static void appendToUcpPath(final Object ucp, final URL url) throws ReflectiveOperationException {
        if (ucp == null) {
            return;
        }
        final Class<?> ucpClass = ucp.getClass();
        final Method addURL = ReflectUtil.getDeclaredMethod(ucpClass, "addURL", URL.class);
        addURL.invoke(ucp, url);
    }
}
