package org.terrydavis.skunforced.injection;

import java.io.IOException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import org.terrydavis.skunforced.launcher.Bootstrap;
import org.terrydavis.skunforced.reflectutils.JavaVersion;

public class JFXInjection
{
    private static final String JFX_CLASSIFIER;
    private static String JFX_VERSION;
    private static List<String> JFX_DEPENDENCY_URLS;
    private static List<Path> dependencyPaths;
    
    // TODO call this!!!
    public static void buildDependencyUrlsList(final String jfxVersion, final List<String> dependencies) {
        JFXInjection.JFX_VERSION = jfxVersion;
        JFXInjection.JFX_DEPENDENCY_URLS = dependencies.stream().map(dependency -> jfxUrlPattern(dependency)).collect(Collectors.toList());
    }
    
    private static void err(Object... objects) {
    	for (Object o : objects) {
    		System.out.print(o);
    		System.out.print(" ");
    	}
    	System.out.println();
    	System.exit(1);
    }
    
    private static void p(Object... objects) {
    	for (Object o : objects) {
    		System.out.print(o);
    		System.out.print(" ");
    	}
    	System.out.println();
    }
    
    public static void ensureJavafxSupport() {
        if (ClasspathUtil.classExists(JFXUtils.getPlatformClassName())) {
            try {
                final Class<?> versionClass = Class.forName("com.sun.javafx.runtime.VersionInfo");
                final Method setupSystemProperties = versionClass.getDeclaredMethod("setupSystemProperties", (Class<?>[])new Class[0]);
                setupSystemProperties.setAccessible(true);
                setupSystemProperties.invoke(null, new Object[0]);
            }
            catch (Exception e) {
                err("Failed to setup JavaFX system properties", e);
            }
            System.out.printf(String.format("JavaFX initialized: %s", System.getProperty("javafx.version")));
            return;
        }
        if (JavaVersion.get() < 11) {
            alertPre11UserMissingJFX();
            return;
        }
        if (JavaVersion.get() >= 9) {
            if (JFXInjection.dependencyPaths == null) {
                getLocalDependencies();
            }
            addToClasspath(JFXInjection.dependencyPaths);
        }
    }
    
    public static boolean areLocalDependenciesValid() {
        try {
            p("Checking local cache for JavaFX dependencies...");
            final Path dependenciesDirectory = Bootstrap.getCurrentInstance().getLibrariesDirectory().toPath();
            for (final String dependencyPattern : JFXInjection.JFX_DEPENDENCY_URLS) {
                final String dependencyUrlPath = String.format(dependencyPattern, JFXInjection.JFX_CLASSIFIER);
                final Path dependencyFilePath = dependenciesDirectory.resolve(getUrlArtifactFileName(dependencyUrlPath));
                final Path dependencyFileHashPath = dependenciesDirectory.resolve(getUrlArtifactFileName(dependencyUrlPath) + ".sha1");
                if (!IOUtils.isRegularFile(dependencyFilePath.toFile(), new LinkOption[0]) || !IOUtils.isRegularFile(dependencyFileHashPath.toFile(), new LinkOption[0])) {
                    err("Local JavaFX dependencies are not valid.");
                    return false;
                }
                final List<String> files = Files.readAllLines(dependencyFileHashPath);
                final String remoteHash = (files.size() > 0) ? files.get(0) : null;
                final String localHash = Utils.sha1(dependencyFilePath.toFile());
                if (!Utils.compareHashes(remoteHash, localHash)) {
                    err("Hash mismatch for " + dependencyFilePath.getFileName());
                    Files.deleteIfExists(dependencyFilePath);
                    Files.deleteIfExists(dependencyFileHashPath);
                    return false;
                }
                p("Local JavaFX dependency " + dependencyFilePath.getFileName() + " is valid.");
            }
        }
        catch (IOException ex) {
            err("Failed to write remote dependency to cache", ex);
            alertUserFailedInit(ex);
        }
        return true;
    }
    
    @SuppressWarnings("unchecked")
	public static List<Path> getLocalDependencies() {
        final List<CompletableFuture<Path>> futures = new ArrayList<CompletableFuture<Path>>();
        try {
            p("Loading local dependencies, and downloading if needed...");
            final Path dependenciesDirectory = Bootstrap.getCurrentInstance().getLibrariesDirectory().toPath();
            final List<Path> oldDependencies = Files.list(dependenciesDirectory).filter(path -> path.getFileName().toString().endsWith(".jar")).collect(Collectors.toList());
            for (final String dependencyPattern : JFXInjection.JFX_DEPENDENCY_URLS) {
                final String dependencyUrlPath = String.format(dependencyPattern, JFXInjection.JFX_CLASSIFIER);
                final String dependencyUrlHashPath = dependencyUrlPath + ".sha1";
                final Path dependencyFilePath = dependenciesDirectory.resolve(getUrlArtifactFileName(dependencyUrlPath));
                final Path dependencyFileHashPath = dependenciesDirectory.resolve(getUrlArtifactFileName(dependencyUrlPath) + ".sha1");
                oldDependencies.remove(dependencyFilePath);
                if (!IOUtils.isRegularFile(dependencyFilePath) || !IOUtils.isRegularFile(dependencyFileHashPath)) {
                    System.out.printf(String.format("Downloading JFX artifact: %s%n", dependencyUrlPath));
                    final URL depURL = new URL(dependencyUrlPath);
                    final URL depHashURL = new URL(dependencyUrlHashPath);
                    // TODO check other decompilers
                    //final URL url;
                    //final Path target;
                    //final URL url2;
                    //final Path target2;
                    futures.add(ThreadUtil.run((Supplier<Path>)Unchecked.supply(() -> {
                        Files.copy(depURL.openStream(), dependencyFilePath, StandardCopyOption.REPLACE_EXISTING);
                        Files.copy(depHashURL.openStream(), dependencyFileHashPath, StandardCopyOption.REPLACE_EXISTING);
                        return dependencyFilePath;
                    })));
                }
                else {
                    futures.add(CompletableFuture.completedFuture(dependencyFilePath));
                }
            }
            if (!oldDependencies.isEmpty()) {
                p("Removing old dependency versions...");
                for (final Path existingDependency : oldDependencies) {
                    Files.delete(existingDependency);
                    Files.deleteIfExists(existingDependency.resolveSibling(existingDependency.getFileName() + ".sha1"));
                }
            }
        }
        catch (MalformedURLException ex) {
            err("Invalid dependency URL path", ex);
            alertUserFailedInit(ex);
        }
        catch (IOException ex2) {
            err("Failed to write remote dependency to cache", ex2);
            alertUserFailedInit(ex2);
        }
        
        //CompletableFuture<?>[] fut = new CompletableFuture<?>[10];
        CompletableFuture<Void> wait = CompletableFuture.allOf((CompletableFuture<Path>[])futures.toArray(new CompletableFuture[0]));
        
        return JFXInjection.dependencyPaths = wait.thenApply(__ -> futures.stream().map(x -> x.getNow(null)).collect(Collectors.toList())).join();
    }
    
    private static void addToClasspath(final List<Path> dependencyPaths) {
        try {
            final Object ucp = ClassLoaderInternals.getUcp();
            for (final Path path : dependencyPaths) {
                final URL url = path.toAbsolutePath().toUri().toURL();
                ClassLoaderInternals.appendToUcpPath(ucp, url);
            }
            p("JavaFX classpath injection complete");
        }
        catch (MalformedURLException ex) {
            p("Failed to resolve local dependency jar to URL", ex);
            alertUserFailedInit(ex);
        }
        catch (ReflectiveOperationException ex2) {
            p("Failed to add missing JavaFX paths to classpath", ex2);
            alertUserFailedInit(ex2);
        }
    }
    
    private static void alertPre11UserMissingJFX() {
    	err("The required JavaFX classes could not be found locally.");
        /*final Toolkit toolkit = Toolkit.getDefaultToolkit();
        toolkit.beep();
        final StringWriter writer = new StringWriter();
        RuntimeProperties.dump(writer);
        final String debugInfo = writer.toString();
        final String style = "<style>p {font-family: Arial; font-size:14;} pre { background: #45494a; padding: 5px; }</style>";
        final String message = "<p>The required JavaFX classes could not be found locally.<br><br>Your environment:</p><pre>" + debugInfo + "</pre><p>You have two options:<br> 1. Use a JDK that bundles JavaFX (like Liberica 8 Full JDK)<br> 2. Update to Java 11 or higher <i>(SKlauncher will automatically download JavaFX)</i></p>";
        final JEditorPane pane = new JEditorPane("text/html", style + message);
        pane.setEditable(false);
        pane.setOpaque(false);
        JOptionPane.showMessageDialog(null, pane, "JavaFX could not be found", 0);
        System.exit(0);*/
    }
    
    private static void alertUserFailedInit(final Exception ex) {
    	err("Something went wrong when trying to load JavaFX.");
    	/*
        final Toolkit toolkit = Toolkit.getDefaultToolkit();
        toolkit.beep();
        final StringWriter writer = new StringWriter();
        writer.append("OS: ").append(System.getProperty("os.name")).append("\n");
        writer.append("Version: ").append(System.getProperty("java.version")).append("\n");
        writer.append("Vendor: ").append(System.getProperty("java.vm.vendor")).append("\n\n");
        writer.append("Exception: ");
        ex.printStackTrace(new PrintWriter(writer));
        final String errorString = writer.toString();
        final StringSelection selection = new StringSelection(errorString);
        final Clipboard clipboard = toolkit.getSystemClipboard();
        clipboard.setContents(selection, selection);
        final String bugReportURL = "https://skmedix.pl/discord";
        final String style = "<style>p {font-family: Arial; font-size:14;} pre {background: #45494a;padding: 5px;border: 1px solid #646464;}</style>";
        final String message = "<p>Something went wrong when trying to load JavaFX.<br><b>The following information about the problem has been copied to your clipboard:</b></p><br><pre>" + errorString + "</pre><p>Please make sure that you meet one of the following requirements:<br> 1. Use a JDK that bundles JavaFX (like Liberica 8 Full JDK)<br> 2. Update to Java 11 or higher <i>(SKlauncher will automatically download JavaFX)</i><br><br>If you believe this is a bug, please <a href=\"" + bugReportURL + "\">write to us on Discord</a>.</p>";
        final JEditorPane pane = new JEditorPane("text/html", style + message);
        pane.setEditable(false);
        pane.setOpaque(false);
        int height = 250 + StringUtils.countMatches(errorString, "\n") * 14;
        if (height > toolkit.getScreenSize().height - 100) {
            height = toolkit.getScreenSize().height - 100;
        }
        final JScrollPane scroll = new JScrollPane(pane);
        scroll.setPreferredSize(new Dimension(800, height));
        scroll.setBorder(BorderFactory.createEmptyBorder());
        final JFrame frame = new JFrame();
        frame.setAlwaysOnTop(true);
        JOptionPane.showMessageDialog(frame, scroll, "Error initializing JavaFX", 0);
        System.exit(1);*/
    }
    
    private static String getUrlArtifactFileName(final String url) {
        return url.substring(url.lastIndexOf(47) + 1);
    }
    
    private static String jfxUrlPattern(final String component) {
        return String.format("https://repo1.maven.org/maven2/org/openjfx/javafx-%s/%s/javafx-%s-%s", component, JFXInjection.JFX_VERSION, component, JFXInjection.JFX_VERSION) + "-%s.jar";
    }
    
    private static String createClassifier() {
        final String os = normalizeOs();
        if (os.equals("win")) {
            return os;
        }
        final String arch = normalizeArch();
        if (os.equals("mac") && arch.equals("aarch64")) {
            return os + "-" + arch;
        }
        if (os.equals("linux") && arch.equals("aarch64")) {
            return os + "-" + arch;
        }
        if (os.equals("linux") && arch.equals("arm32")) {
            return os + "-" + arch;
        }
        return os;
    }
    
    private static String normalizeOs() {
        final String os = normalize(RuntimeProperties.OS_NAME);
        if (os.startsWith("macosx") || os.startsWith("osx")) {
            return "mac";
        }
        if (os.startsWith("win")) {
            return "win";
        }
        return "linux";
    }
    
    private static String normalizeArch() {
        final String arch = normalize(RuntimeProperties.OS_ARCH);
        if ("aarch64".equals(arch)) {
            return "aarch64";
        }
        if (arch.matches("^(arm|arm32)$")) {
            return "arm32";
        }
        return arch;
    }
    
    private static String normalize(final String value) {
        return value.toLowerCase().replaceAll("[^a-z0-9]+", "");
    }
    
    static {
        JFX_CLASSIFIER = createClassifier();
        JFXInjection.JFX_DEPENDENCY_URLS = new ArrayList<String>();
    }
}

