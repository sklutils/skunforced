package org.terrydavis.skunforced.injection;

import java.io.StringWriter;
import java.util.Map;
import java.util.TreeMap;

@SuppressWarnings({ "unused", "serial" })
public class RuntimeProperties
{
    private static final String KEY_OS_NAME = "os.name";
    private static final String KEY_OS_ARCH = "os.arch";
    private static final String KEY_OS_VERSION = "os.version";
    private static final String KEY_OS_ARCH_BITS = "os.bitness";
    private static final String KEY_JAVA_VERSION = "java.version";
    private static final String KEY_JAVA_VM_NAME = "java.vm.version";
    private static final String KEY_JAVA_VM_VENDOR = "java.vm.vendor";
	private static final String KEY_JAVA_HOME = "java.home";
    public static final String OS_NAME;
    public static final String OS_ARCH;
    public static final int OS_ARCH_BITS;
    public static final String OS_VERSION;
    public static final String JAVA_VERSION;
    public static final String JAVA_VM_NAME;
    public static final String JAVA_VM_VENDOR;
    public static final String JAVA_HOME;
    private static final Map<String, String> ALL_PROPERTIES;
    
    private static int determineBitness() {
        String bitness = System.getProperty("sun.arch.data.model", "");
        if (bitness.matches("[0-9]{2}")) {
            return Integer.parseInt(bitness, 10);
        }
        bitness = System.getProperty("com.ibm.vm.bitmode", "");
        if (bitness.matches("[0-9]{2}")) {
            return Integer.parseInt(bitness, 10);
        }
        return RuntimeProperties.OS_ARCH.contains("64") ? 64 : 32;
    }
    
    public static void dump(final StringWriter writer) {
        if (writer != null) {
            RuntimeProperties.ALL_PROPERTIES.forEach((key, value) -> writer.append(String.format("%s = %s\n", key, value)));
        }
    }
    
    static {
        OS_NAME = System.getProperty("os.name");
        OS_ARCH = System.getProperty("os.arch");
        OS_ARCH_BITS = determineBitness();
        OS_VERSION = System.getProperty("os.version");
        JAVA_VERSION = System.getProperty("java.version");
        JAVA_VM_NAME = System.getProperty("java.vm.version");
        JAVA_VM_VENDOR = System.getProperty("java.vm.vendor");
        JAVA_HOME = System.getProperty("java.home");
        ALL_PROPERTIES = new TreeMap<String, String>() {
            {
                this.put("os.name", RuntimeProperties.OS_NAME);
                this.put("os.arch", RuntimeProperties.OS_ARCH);
                this.put("os.bitness", String.valueOf(RuntimeProperties.OS_ARCH_BITS));
                this.put("os.version", RuntimeProperties.OS_VERSION);
                this.put("java.version", RuntimeProperties.JAVA_VERSION);
                this.put("java.vm.version", RuntimeProperties.JAVA_VM_NAME);
                this.put("java.vm.vendor", RuntimeProperties.JAVA_VM_VENDOR);
                this.put("java.home", RuntimeProperties.JAVA_HOME);
            }
        };
    }
}