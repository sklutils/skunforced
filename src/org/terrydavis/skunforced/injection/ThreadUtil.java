package org.terrydavis.skunforced.injection;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ScheduledExecutorService;
import java.util.function.Supplier;

public class ThreadUtil
{
    private static final ScheduledExecutorService scheduledService;
    
    public static <T> CompletableFuture<T> run(final Supplier<T> action) {
        return CompletableFuture.supplyAsync(action, ThreadUtil.scheduledService);
    }
    
    static {
        scheduledService = ThreadPoolFactory.newScheduledThreadPool("SKL misc");
    }
}

