package org.terrydavis.skunforced.injection;

import java.util.function.Supplier;

import org.terrydavis.skunforced.reflectutils.ReflectUtil;

@FunctionalInterface
public interface UncheckedSupplier<T> extends Supplier<T>
{
    default T get() {
        try {
            return this.uncheckedGet();
        }
        catch (Throwable t) {
            try {
				ReflectUtil.propagate(t);
			} catch (Throwable e) {
				e.printStackTrace();
			}
            return null;
        }
    }
    
    T uncheckedGet() throws Throwable;
}
