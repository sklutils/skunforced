package org.terrydavis.skunforced.injection;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;

public class IOUtils
{
    public static String toString(final InputStream input) throws IOException {
        final StringBuilderWriter sw = new StringBuilderWriter();
        copy(input, sw);
        return sw.toString();
    }
    
    private static void copy(final InputStream input, final Writer output) throws IOException {
        final InputStreamReader in = new InputStreamReader(input, StandardCharsets.UTF_8);
        copy(in, output);
    }
    
    private static int copy(final Reader input, final Writer output) throws IOException {
        final long count = copyLarge(input, output);
        if (count > 2147483647L) {
            return -1;
        }
        return (int)count;
    }
    
    private static long copyLarge(final Reader input, final Writer output) throws IOException {
        return copyLarge(input, output, new char[4096]);
    }
    
    private static long copyLarge(final Reader input, final Writer output, final char[] buffer) throws IOException {
        long count = 0L;
        int n;
        while (-1 != (n = input.read(buffer))) {
            output.write(buffer, 0, n);
            count += n;
        }
        return count;
    }
    
    public static int copy(final InputStream inputStream, final OutputStream outputStream) throws IOException {
        final long count = copyLarge(inputStream, outputStream);
        if (count > 2147483647L) {
            return -1;
        }
        return (int)count;
    }
    
    public static long copyLarge(final InputStream inputStream, final OutputStream outputStream) throws IOException {
        return copy(inputStream, outputStream, 8192);
    }
    
    public static long copy(final InputStream inputStream, final OutputStream outputStream, final int bufferSize) throws IOException {
        return copyLarge(inputStream, outputStream, byteArray(bufferSize));
    }
    
    public static long copyLarge(final InputStream inputStream, final OutputStream outputStream, final byte[] buffer) throws IOException {
        long count = 0L;
        int n;
        while (-1 != (n = inputStream.read(buffer))) {
            outputStream.write(buffer, 0, n);
            count += n;
        }
        return count;
    }
    
    public static byte[] byteArray(final int size) {
        return new byte[size];
    }
    
    public static boolean isRegularFile(final File file, final LinkOption... options) {
        return file != null && Files.isRegularFile(file.toPath(), options);
    }
    
    public static boolean isRegularFile(final Path path) {
        return isOnDefaultFileSystem(path) ? path.toFile().isFile() : Files.isRegularFile(path, new LinkOption[0]);
    }
    
    public static boolean isOnDefaultFileSystem(final Path path) {
        return path.getFileSystem() == FileSystems.getDefault();
    }
    
    public static class StringBuilderWriter extends Writer implements Serializable
    {
        private static final long serialVersionUID = -146927496096066153L;
        private final StringBuilder builder;
        
        public StringBuilderWriter() {
            this.builder = new StringBuilder();
        }
        
        @Override
        public Writer append(final char value) {
            this.builder.append(value);
            return this;
        }
        
        @Override
        public Writer append(final CharSequence value) {
            this.builder.append(value);
            return this;
        }
        
        @Override
        public Writer append(final CharSequence value, final int start, final int end) {
            this.builder.append(value, start, end);
            return this;
        }
        
        @Override
        public void close() {
        }
        
        @Override
        public void flush() {
        }
        
        @Override
        public void write(final String value) {
            if (value != null) {
                this.builder.append(value);
            }
        }
        
        @Override
        public void write(final char[] value, final int offset, final int length) {
            if (value != null) {
                this.builder.append(value, offset, length);
            }
        }
        
        @Override
        public String toString() {
            return this.builder.toString();
        }
    }
}

