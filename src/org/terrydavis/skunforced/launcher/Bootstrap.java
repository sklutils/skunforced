package org.terrydavis.skunforced.launcher;

import java.io.File;
import java.lang.reflect.Constructor;
import java.net.MalformedURLException;
import java.net.PasswordAuthentication;
import java.net.Proxy;
import java.net.URL;
import java.net.URLClassLoader;

import org.terrydavis.skunforced.injection.JFXInjection;
import org.terrydavis.skunforced.reflectutils.AccessPatcher;

public class Bootstrap {

	private static Bootstrap instance;
	private File librariesDirectory;
	private File launcherJar;
	private File workingDirectory;
	
	@SuppressWarnings("unused")
	private String[] remainderArgs = new String[] {};
	
	public static final Integer VERR = 300;
	
	public Bootstrap() {
		// default constructor
		instance = this;
		File dir = new File("./skl");
		if (!dir.exists() || !dir.isDirectory()) {
			dir.mkdir();
		}
		this.librariesDirectory = new File("./skl/libs");
		if (!this.librariesDirectory.exists() || !this.librariesDirectory.isDirectory()) {
			this.librariesDirectory.mkdir();
		}
		this.workingDirectory = new File("./skl/minecraft");
		if (!this.workingDirectory.exists() || !this.workingDirectory.isDirectory()) {
			this.workingDirectory.mkdir();
		}
		this.launcherJar = new File("./skl/sklauncher-fx.jar");
		if (!this.launcherJar.exists() || !this.launcherJar.isFile()) {
			throw new RuntimeException("Failed to find file: " + this.launcherJar.getAbsolutePath());
		}
	}
	
    public static Bootstrap getCurrentInstance() {
        return Bootstrap.instance;
    }

    public File getLibrariesDirectory() {
        return this.librariesDirectory;
    }
    
	public void startLauncher(String[] args) throws Exception {
        if (!this.launcherJar.exists()) {
            throw new Exception("Unable to start: Launcher JAR does not exist");
        }
        AccessPatcher.patch();
        JFXInjection.ensureJavafxSupport();
        try {
            final Class<?> launcher = this.extract().loadClass("net.minecraft.launcher.Launcher");
            final Constructor<?> c = launcher.getConstructor(File.class, Proxy.class, PasswordAuthentication.class, String[].class, Integer.class, Boolean.TYPE);
            c.newInstance(this.workingDirectory, Proxy.NO_PROXY, null, args, VERR, false);
        }
        catch (Exception e) {
        	e.printStackTrace();
            //AlertUtils.displayRawException("Fatal error", StringUtils.getStackTrace(e));
            System.exit(1);
        }
    }
    
    private URLClassLoader extract() throws MalformedURLException {
        return new URLClassLoader(new URL[] { this.launcherJar.toURI().toURL() });
    }
    
}
