/*
 * NOTICE
 * 
 * If you are reading this code do know that most of java files in this project
 * come from decompiling the original sklauncher.exe. as such, most of these
 * were not rewritten and contain zero comments and might look confusing.
 * they were only edited to the point of compilation being successful, so
 * parts of the code may look weird!
 * This applies mostly to classes in injection and reflectutils package.
 * */
package org.terrydavis.skunforced;

import java.util.Arrays;
import org.terrydavis.skunforced.injection.JFXInjection;
import org.terrydavis.skunforced.launcher.Bootstrap;

public class Main {

	// TODO change this one day
	public static final String JFX_VERR = "19.0.2.1";
	public static final String[] JFX_MOD = new String[] { "web", "swing", "media", "controls", "graphics", "base" };

	// entry point
	public static void main(String[] args) throws Exception {
		if (args.length == 1 && args[0].equalsIgnoreCase("initonly")) {
			try {
				new Bootstrap();				
			}
			catch (RuntimeException e) {
				System.out.println(e.getMessage());
				System.out.println(""
						+ "This error most likely occured because the file "
						+ "sklauncher-fx.jar is missing in the skl directory. "
						+ "You can get this file on project's gitlab. "
						+ "Additionally it is available on (at time of writing) "
						+ "https://files.skmedix.pl/launcher.pack. This however "
						+ "is not the jar you are looking for. It is a compressed file "
						+ "using XZInputStream from tuukani (https://tukaani.org/xz/)."
						+ "after extraction you should be able to use that jar in this launcher.");
				System.exit(1);
			}
			
			System.out.println("Everything is ready!");
			System.exit(0);
		}
		JFXInjection.buildDependencyUrlsList(JFX_VERR, Arrays.asList(JFX_MOD));
		new Bootstrap().startLauncher(args);
	}

}
