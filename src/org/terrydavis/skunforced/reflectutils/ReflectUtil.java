package org.terrydavis.skunforced.reflectutils;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

public final class ReflectUtil
{
    private static final Map<Class<?>, ThrowableGetter<?>> GETTERS;
    private static final Map<Class<?>, ThrowableSetter<?>> SETTERS;
    private static final ThrowableGetter<?> DEFAULT_GETTER;
    private static final ThrowableSetter<?> DEFAULT_SETTER;
    
    private ReflectUtil() {
    }
    
    public static Field getDeclaredField(final Class<?> declaringClass, final String name) throws NoSuchFieldException {
        final Field field = declaringClass.getDeclaredField(name);
        field.setAccessible(true);
        return field;
    }
    
    public static Method getDeclaredMethod(final Class<?> declaringClass, final String name, final Class<?>... args) throws NoSuchMethodException {
        final Method method = declaringClass.getDeclaredMethod(name, args);
        method.setAccessible(true);
        return method;
    }
    
    public static <T> void quietSet(final Object instance, final Field field, final T value) {
        try {
            @SuppressWarnings("unchecked")
			final ThrowableSetter<T> setter = (ThrowableSetter<T>)ReflectUtil.SETTERS.getOrDefault(field.getType(), ReflectUtil.DEFAULT_SETTER);
            setter.set(field, instance, value);
        }
        catch (IllegalAccessException ex) {
            throw new IllegalStateException("Setter failure: " + instance.getClass() + "." + field.getName(), ex);
        }
    }
    
    public static <T> T quietGet(final Object instance, final Field field) {
        try {
            @SuppressWarnings("unchecked")
			final ThrowableGetter<T> getter = (ThrowableGetter<T>)ReflectUtil.GETTERS.getOrDefault(field.getType(), ReflectUtil.DEFAULT_GETTER);
            return getter.get(field, instance);
        }
        catch (IllegalAccessException ex) {
            throw new IllegalStateException("Getter failure: " + instance.getClass() + "." + field.getName(), ex);
        }
    }
    
    public static <T> T quietNew(final Class<T> type, final Class<?>[] argTypes, final Object[] args) {
        try {
            final Constructor<T> constructor = type.getDeclaredConstructor(argTypes);
            constructor.setAccessible(true);
            return constructor.newInstance(args);
        }
        catch (ReflectiveOperationException ex) {
            throw new IllegalStateException("Constructor failure: " + type.getName(), ex);
        }
    }
    
    @SuppressWarnings("unchecked")
	public static <T> T quietInvoke(final Class<?> type, final Object instance, final String name, final Class<?>[] argTypes, final Object[] args) {
        try {
            final Method method = type.getDeclaredMethod(name, argTypes);
            method.setAccessible(true);
            return (T)method.invoke(instance, args);
        }
        catch (ReflectiveOperationException ex) {
            throw new IllegalStateException("Invoke failure: " + type.getName(), ex);
        }
    }
    
    public static void copyTo(final Object from, final Object to) {
        if (from == null || to == null) {
            return;
        }
        final Class<?> type = to.getClass();
        if (!type.equals(from.getClass())) {
            return;
        }
        for (final Field field : type.getDeclaredFields()) {
            if ((field.getModifiers() & 0x8) <= 0) {
                field.setAccessible(true);
                final Object value = quietGet(from, field);
                quietSet(to, field, value);
            }
        }
    }
    
    public static <X extends Throwable> void propagate(final Throwable t) throws X, Throwable {
        throw t;
    }
    
    private static Object get(final Field field, final Object instance) throws IllegalAccessException {
        field.setAccessible(true);
        return field.get(instance);
    }
    
    private static void set(final Field field, final Object instance, final Object value) throws IllegalAccessException {
        field.setAccessible(true);
        field.set(instance, value);
    }
    
    static {
        GETTERS = new HashMap<Class<?>, ThrowableGetter<?>>();
        SETTERS = new HashMap<Class<?>, ThrowableSetter<?>>();
        DEFAULT_GETTER = ReflectUtil::get;
        DEFAULT_SETTER = ReflectUtil::set;
        ReflectUtil.GETTERS.put(Boolean.TYPE, Field::getBoolean);
        ReflectUtil.GETTERS.put(Byte.TYPE, Field::getByte);
        ReflectUtil.GETTERS.put(Character.TYPE, Field::getChar);
        ReflectUtil.GETTERS.put(Short.TYPE, Field::getShort);
        ReflectUtil.GETTERS.put(Integer.TYPE, Field::getInt);
        ReflectUtil.GETTERS.put(Long.TYPE, Field::getLong);
        ReflectUtil.GETTERS.put(Float.TYPE, Field::getFloat);
        ReflectUtil.GETTERS.put(Double.TYPE, Field::getDouble);
        ReflectUtil.SETTERS.put(Boolean.TYPE, new ThrowableSetter<Boolean>() {
			@Override
			public void set(Field p0, Object p1, Boolean p2)
					throws IllegalAccessException {
				p0.setBoolean(p1, p2);
			}
		});
        ReflectUtil.SETTERS.put(Byte.TYPE, new ThrowableSetter<Byte>() {
			@Override
			public void set(Field p0, Object p1, Byte p2) throws IllegalAccessException {
				p0.setByte(p1, p2);
			}
		});
        ReflectUtil.SETTERS.put(Character.TYPE, new ThrowableSetter<Character>() {
			@Override
			public void set(Field p0, Object p1, Character p2) throws IllegalAccessException {
				p0.setChar(p1, p2);
			}
		});
        ReflectUtil.SETTERS.put(Short.TYPE, new ThrowableSetter<Short>() {
			@Override
			public void set(Field p0, Object p1, Short p2) throws IllegalAccessException {
				p0.setShort(p1, p2);
			}
		});
        ReflectUtil.SETTERS.put(Integer.TYPE, new ThrowableSetter<Integer>() {
			@Override
			public void set(Field p0, Object p1, Integer p2) throws IllegalAccessException {
				p0.setInt(p1, p2);
			}
		});
        ReflectUtil.SETTERS.put(Long.TYPE, new ThrowableSetter<Long>() {
			@Override
			public void set(Field p0, Object p1, Long p2) throws IllegalAccessException {
				p0.setLong(p1, p2);
			}
		});
        ReflectUtil.SETTERS.put(Float.TYPE, new ThrowableSetter<Float>() {
			@Override
			public void set(Field p0, Object p1, Float p2) throws IllegalAccessException {
				p0.setFloat(p1, p2);
			}
		});
        ReflectUtil.SETTERS.put(Double.TYPE, new ThrowableSetter<Double>() {
			@Override
			public void set(Field p0, Object p1, Double p2) throws IllegalAccessException {
				p0.setDouble(p1, p2);
			}
		});
    }
    
    interface ThrowableGetter<T>
    {
        T get(final Field p0, final Object p1) throws IllegalAccessException;
    }
    
    interface ThrowableSetter<T>
    {
        void set(final Field p0, final Object p1, final T p2) throws IllegalAccessException;
    }
}
