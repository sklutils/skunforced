package org.terrydavis.skunforced.reflectutils;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;

public class AccessPatcher
{
    private AccessPatcher() {
    }
    
    public static void patch() {
        final int version = JavaVersion.get();
        if (version > 8) {
            try {
                System.out.println("Opening access to all packages");
                openPackages();
                System.out.println("Patching package reflection restrictions");
                patchReflectionFilters();
            }
            catch (Throwable t) {
                System.out.printf("Failed access patching on Java %d: %s%n", version, t.toString());
            }
        }
    }
    
    private static void openPackages() {
        try {
            final Method export = Module.class.getDeclaredMethod("implAddOpens", String.class);
            JigsawUtil.setMethodModifiers(export, 1);
            final HashSet<Module> modules = new HashSet<Module>();
            final Class<?> classBase = JigsawUtil.class;
            final Module base = JigsawUtil.getClassModule(classBase);
            if (base.getLayer() != null) {
                modules.addAll((Collection<? extends Module>)base.getLayer().modules());
            }
            modules.addAll((Collection<? extends Module>)ModuleLayer.boot().modules());
            for (ClassLoader cl = classBase.getClassLoader(); cl != null; cl = cl.getParent()) {
                modules.add(JigsawUtil.getLoaderModule(cl));
            }
            for (final Module module : modules) {
                for (final String name : module.getPackages()) {
                    try {
                        export.invoke(module, name);
                    }
                    catch (Exception ex) {
                        System.out.println(String.format("Could not export package %s in module %s", name, module));
                        System.out.printf("Root cause: %s%n", ex.toString());
                    }
                }
            }
        }
        catch (Exception ex2) {
            throw new IllegalStateException("Could not export packages", ex2);
        }
    }
    
    @SuppressWarnings("rawtypes")
	private static void patchReflectionFilters() {
        Class<?> klass;
        try {
            klass = Class.forName("jdk.internal.reflect.Reflection", true, null);
        }
        catch (ClassNotFoundException ex) {
            throw new RuntimeException("Unable to locate 'jdk.internal.reflect.Reflection' class", ex);
        }
        try {
            Field[] fields;
            try {
                final Method m = Class.class.getDeclaredMethod("getDeclaredFieldsImpl", (Class<?>[])new Class[0]);
                m.setAccessible(true);
                fields = (Field[])m.invoke(klass, new Object[0]);
            }
            catch (NoSuchMethodException | InvocationTargetException ex7) {
                //final ReflectiveOperationException ex5;
                //final ReflectiveOperationException ex2 = ex5;
                try {
                    final Method i = Class.class.getDeclaredMethod("getDeclaredFields0", Boolean.TYPE);
                    i.setAccessible(true);
                    fields = (Field[])i.invoke(klass, false);
                }
                catch (InvocationTargetException | NoSuchMethodException ex8) {
                    //final ReflectiveOperationException ex6;
                    //final ReflectiveOperationException ex3 = ex6;
                    //ex2.addSuppressed(ex3);
                	// TODO add ex2
                    throw new RuntimeException("Unable to get all class fields");
                }
            }
            int c = 0;
            for (final Field field : fields) {
                final String name = field.getName();
                if ("fieldFilterMap".equals(name) || "methodFilterMap".equals(name)) {
                    field.setAccessible(true);
                    field.set(null, new HashMap(0));
                    if (++c == 2) {
                        return;
                    }
                }
            }
            throw new RuntimeException("One of field patches did not apply properly. Expected to patch two fields, but patched: " + c);
        }
        catch (IllegalAccessException ex4) {
            throw new RuntimeException("Unable to patch reflection filters", ex4);
        }
    }
}
