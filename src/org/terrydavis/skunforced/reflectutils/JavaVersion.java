package org.terrydavis.skunforced.reflectutils;

public class JavaVersion
{
    public static final int VERSION_OFFSET = 44;
    //private static final String JAVA_CLASS_VERSION = "java.class.version";
    //private static final String JAVA_VM_SPEC_VERSION = "java.vm.specification.version";
    //private static final int FALLBACK_VERSION = 8;
    private static int version;
    
    public static int get() {
        if (JavaVersion.version >= 0) {
            return JavaVersion.version;
        }
        String property = System.getProperty("java.class.version", "");
        if (!property.isEmpty()) {
            return JavaVersion.version = (int)(Float.parseFloat(property) - 44.0f);
        }
        System.out.printf(String.format("Property '%s' not found, using '%s' as fallback", "java.class.version", "java.vm.specification.version"));
        property = System.getProperty("java.vm.specification.version", "");
        if (property.contains(".")) {
            return JavaVersion.version = (int)Float.parseFloat(property.substring(property.indexOf(46) + 1));
        }
        if (!property.isEmpty()) {
            return JavaVersion.version = Integer.parseInt(property);
        }
        System.out.printf(String.format("Property '%s' not found, using '%s' as fallback", "java.vm.specification.version", 8));
        return 8;
    }
    
    static {
        JavaVersion.version = -1;
    }
}
