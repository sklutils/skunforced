package org.terrydavis.skunforced.reflectutils;

import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.invoke.MethodType;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class JigsawUtil
{
    private static final MethodHandle CLASS_MODULE;
    private static final MethodHandle CLASS_LOADER_MODULE;
    private static final MethodHandle METHOD_MODIFIERS;
    private static final MethodHandles.Lookup lookup;
    
    private JigsawUtil() {
    }
    
    private static void p(Object... objects) {
    	for (Object o : objects) {
    		System.out.print(o);
    	}
    	System.out.println();
    }
    
    public static MethodHandles.Lookup getLookup() {
        return JigsawUtil.lookup;
    }
    
    static Module getClassModule(final Class<?> klass) {
        try {
            return (Module) JigsawUtil.CLASS_MODULE.invokeExact((Class<?>)klass);
        }
        catch (Throwable t) {
            p("Failed getting class module: " + klass.getName(), t);
            throw new AssertionError((Object)t);
        }
    }
    
    static Module getLoaderModule(final ClassLoader loader) {
        try {
            return (Module) JigsawUtil.CLASS_LOADER_MODULE.invokeExact(loader);
        }
        catch (Throwable t) {
            p("Failed getting ClassLoader module: " + loader, t);
            throw new AssertionError((Object)t);
        }
    }
    
    static void setMethodModifiers(final Method method, final int modifiers) {
        try {
            JigsawUtil.METHOD_MODIFIERS.invokeExact(method, modifiers);
        }
        catch (Throwable t) {
            p("Failed setting method modifiers: " + method.getName(), t);
            throw new AssertionError((Object)t);
        }
    }
    
    static {
        try {
            final Class<?> unsafeClass = Class.forName("sun.misc.Unsafe");
            Field field = ReflectUtil.getDeclaredField(unsafeClass, "theUnsafe");
            final Object unsafe = field.get(null);
            field = MethodHandles.Lookup.class.getDeclaredField("IMPL_LOOKUP");
            MethodHandles.publicLookup();
            final Object base = ReflectUtil.quietInvoke(unsafeClass, unsafe, "staticFieldBase", new Class[] { Field.class }, new Object[] { field });
            final long offset = ReflectUtil.quietInvoke(unsafeClass, unsafe, "staticFieldOffset", new Class[] { Field.class }, new Object[] { field });
            lookup = ReflectUtil.quietInvoke(unsafeClass, unsafe, "getObject", new Class[] { Object.class, Long.TYPE }, new Object[] { base, offset });
            final MethodType type = MethodType.methodType(Module.class);
            CLASS_MODULE = JigsawUtil.lookup.findVirtual(Class.class, "getModule", type);
            CLASS_LOADER_MODULE = JigsawUtil.lookup.findVirtual(ClassLoader.class, "getUnnamedModule", type);
            METHOD_MODIFIERS = JigsawUtil.lookup.findSetter(Method.class, "modifiers", Integer.TYPE);
        }
        catch (ClassNotFoundException | NoSuchMethodException | IllegalAccessException | NoSuchFieldException ex3) {
            //final ReflectiveOperationException ex2;
            //final ReflectiveOperationException ex = ex2;
            throw new ExceptionInInitializerError(ex3);
        }
    }
}

